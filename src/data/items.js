export default [
  {
    id: 1,
    name: 'Apple watch',
    image: 'img/apple-watch.jpg',
    price: 32300,
    category: 1,
    colors: [
      '#73B6EA',
      '#FFF',
      '#000',
    ],
  },
  {
    id: 2,
    name: 'Пленка для самсунг',
    image: 'img/plenka-samsung.jpg',
    price: 200,
    category: 1,
    colors: [
      '#000',
    ],
  },
  {
    id: 3,
    name: 'Кот гопник',
    image: 'img/cat-gopnik.jpg',
    price: 50,
    category: 2,
    colors: [
      '#FFF',
      '#000',
    ],
  },
  {
    id: 4,
    name: 'Пицца',
    image: 'img/pizza.jpg',
    price: 750,
    category: 3,
    colors: [
      '#73B6EA',
      '#FF6B00',
      '#FFF',
      '#000',
    ],
  },
  {
    id: 5,
    name: 'Старый мем',
    image: 'img/old-meme.jpg',
    price: 1,
    category: 2,
    colors: [
      '#73B6EA',

    ],
  },
  {
    id: 6,
    name: 'Грустный демотиватор',
    image: 'img/statham.jpg',
    price: 15,
    category: 2,
    colors: [
      '#73B6EA',
      '#000',
    ],
  },
  {
    id: 7,
    name: 'Носки с авокадо',
    image: 'img/socks.jpg',
    category: 4,
    price: 250,
    colors: [
      '#73B6EA',
      '#FFBE15',
    ],
  },
  {
    id: 8,
    name: 'Скейт',
    image: 'img/skate.jpg',
    category: 5,
    price: 4200,
    colors: [
      '#73B6EA',
      '#FFBE15',
      '#939393',
      '#FF6B00',
      '#FFF',
      '#000',
    ],
  },
  {
    id: 9,
    name: 'Лестница',
    image: 'img/ladder.jpg',
    category: 5,
    price: 3200,
    colors: [
      '#73B6EA',
      '#FFBE15',
      '#FF6B00',
      '#FFF',
      '#000',
    ],
  },

];
