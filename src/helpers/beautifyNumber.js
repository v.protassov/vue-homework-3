export default function (value) {
  if (typeof value === 'number') {
    return new Intl.NumberFormat().format(value);
  }
  return value;
}
