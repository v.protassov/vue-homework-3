import eventBus from '@/eventBus';

export default function (pageName, pageData) {
  eventBus.$emit('changePage', pageName, pageData);
}
