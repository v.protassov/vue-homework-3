import Vue from 'vue';
// import { data, someData } from '@/data/data';
// import performAlert from '@/utils';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

// performAlert(someData);
// performAlert(data);
