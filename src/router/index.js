import Vue from 'vue';
import VueRouter from 'vue-router';
import Catalog from '@/views/Catalog.vue';
import NotFound from '@/views/NotFound.vue';
import ItemPage from '@/views/ItemPage.vue';
import CartPage from '@/views/CartPage.vue';

Vue.use(VueRouter);

const routes = [
  { path: '/', component: Catalog, name: 'main' },
  { path: '/item/:id/', component: ItemPage, name: 'item' },
  { path: '/cart/', component: CartPage, name: 'cart' },
  { path: '*', component: NotFound, name: 'notFound' },

];

const router = new VueRouter({
  routes,
});

export default router;
