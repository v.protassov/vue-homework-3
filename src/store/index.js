import Vue from 'vue';
import Vuex from 'vuex';
import items from '@/data/items';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    itemsInCart: [],
  },
  mutations: {
    addItemToCart(state, { itemId, quantity }) {
      const oldData = state.itemsInCart.find((item) => item.itemId === itemId);
      console.log(oldData);
      if (oldData) {
        oldData.quantity += quantity;
      } else {
        state.itemsInCart.push({
          itemId,
          quantity,
        });
      }
    },
    changeQuantity(state, { itemId, quantity }) {
      const oldData = state.itemsInCart.find((item) => item.itemId === itemId);
      if (oldData) {
        oldData.quantity = quantity;
      }
    },
    removeItemFromCart(state, { itemId }) {
      state.itemsInCart = state.itemsInCart.filter((item) => item.itemId !== itemId);
    },
  },
  actions: {},
  modules: {},
  getters: {
    totalItems(state) {
      return state.itemsInCart.length;
    },
    itemsWithInfo(state) {
      return state.itemsInCart.map((item) => (
        { ...item, ...items.find((it) => it.id === item.itemId) }));
    },
  },
});
